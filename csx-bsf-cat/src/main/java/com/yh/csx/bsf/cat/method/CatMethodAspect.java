package com.yh.csx.bsf.cat.method;

import com.dianping.cat.Cat;
import com.dianping.cat.message.Transaction;
import com.yh.csx.bsf.cat.CatProperties;
import com.yh.csx.bsf.core.util.PropertyUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

/**
 * @创建人 霍钧城
 * @创建时间 2020年12月25日 10:38:00
 * @描述
 */
@Aspect
public class CatMethodAspect {
    @Pointcut("@annotation(com.yh.csx.bsf.cat.method.CatMethod)")
    public void catMethod() {
    }

    /**
     * @描述 自定义CAT拦截
     * @参数 [joinPoint]
     * @返回值 java.lang.Object
     * @创建人 霍钧城
     * @创建时间 2020/12/25
     * @修改历史：
     */
    @Around("catMethod()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        Object returnObj = null;
        if (!PropertyUtils.getPropertyCache(CatProperties.CatEnabled, false)) {
            return joinPoint.proceed();
        } else {
            MethodSignature joinPointObject = (MethodSignature) joinPoint.getSignature();
            Method method = joinPointObject.getMethod();
            CatMethod catMethod = method.getAnnotation(CatMethod.class);
            Transaction t = Cat.newTransaction(catMethod.value(), method.getDeclaringClass().getName() + "." + method.getName());
            try {
                returnObj = joinPoint.proceed();
                t.setStatus(Transaction.SUCCESS);
            } catch (Throwable throwable) {
                Cat.logError(throwable);
                throw throwable;
            } finally {
                t.complete();
            }
            return returnObj;
        }
    }
}

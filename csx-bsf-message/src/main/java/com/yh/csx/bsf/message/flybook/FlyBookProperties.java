package com.yh.csx.bsf.message.flybook;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: huojuncheng
 * @version: 2020-08-25 15:10
 **/
@ConfigurationProperties
public class FlyBookProperties {
    public static String Domain="https://open.feishu.cn/open-apis/bot/hook/";
    public static String flyBookUrl="bsf.message.flybook.url";
}

package com.yh.csx.bsf.file;

/**
 * @author huojuncheng
 */
public interface FileProvider {
    /**
     *@描述 带缩略功能的图片上传
     *@参数  [filePath（绝对路径）, isTemp（保存30天）]
     *@返回值  java.lang.String
     *@创建人  霍钧城
     *@创建时间  2020/12/21 
     *@修改历史：
     */
    String uploadProcessImage(String filePath,boolean isTemp);
    /**
     *@描述 普通文件上传
     *@参数  [filePath（绝对路径）, isTemp（保存30天）]
     *@返回值  java.lang.String
     *@创建人  霍钧城
     *@创建时间  2020/12/21
     *@修改历史：
     */
    String uploadFile(String filePath,boolean isTemp);


}

package com.yh.csx.bsf.file.impl;

import com.yh.csx.bsf.core.base.BsfException;
import com.yh.csx.bsf.core.util.FailOverUtils;
import com.yh.csx.bsf.core.util.ContextUtils;
import com.yh.csx.bsf.core.util.ExceptionUtils;
import com.yh.csx.bsf.health.base.AbstractCollectTask;
import com.yh.csx.bsf.health.base.EnumWarnType;

/**
 * @author huojuncheng
 */
public class BaseFileProvider extends AbstractFileProvider {
    /**
     *@描述 七牛云上传图片
     *@参数  [filePath, path, name]
     *@返回值  java.lang.String
     *@创建人  霍钧城
     *@创建时间  2020/12/21 
     *@修改历史：
     */
    @Override
    public String uploadProcessImage(String filePath, String path) {
        return FailOverUtils.invoke(this::invokeResult,
                () -> ContextUtils.getBean(QiniuFileProvider.class, true).upload(filePath, path));
    }
    /**
     *@描述 文件上传，腾讯为主，七牛云补偿
     *@参数  [filePath, path, name]
     *@返回值  java.lang.String
     *@创建人  霍钧城
     *@创建时间  2020/12/21
     *@修改历史：
     */
    @Override
    public String uploadFile(String filePath, String path) {
        return FailOverUtils.invoke(this::invokeResult,
                () -> ContextUtils.getBean(TencentFileProvider.class, true).upload(filePath, path),
                () -> ContextUtils.getBean(QiniuFileProvider.class, true).upload(filePath, path));
    }
    /**
     *@描述 补偿失败，异常处理
     *@参数  [r]
     *@返回值  void
     *@创建人  霍钧城
     *@创建时间  2020/12/21
     *@修改历史：
     */
    private  void invokeResult(FailOverUtils.Result r) {
        if (!r.isSuccess()) {
            AbstractCollectTask.notifyMessage(EnumWarnType.ERROR, "上传失败", ExceptionUtils.getFullStackTrace(r.getThrowable()));
            throw new BsfException(r.getThrowable());
        }
    }
}

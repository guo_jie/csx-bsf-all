package com.yh.csx.bsf.file.impl;


import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.region.Region;
import com.qiniu.util.StringUtils;
import com.yh.csx.bsf.core.util.LogUtils;
import com.yh.csx.bsf.core.util.PropertyUtils;
import com.yh.csx.bsf.file.FileException;
import com.yh.csx.bsf.file.config.FileProperties;
import com.yh.csx.bsf.file.config.TencentProperties;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.InputStream;
import java.util.Date;

/**
 * @author huojuncheng
 */
@Slf4j
public class TencentFileProvider extends BaseFileProvider {
    private COSClient cosClient;
    private COSCredentials auth;
    private ClientConfig clientConfig;
    private String bucketName;

    public TencentFileProvider(TencentProperties properties) {
        if (StringUtils.isNullOrEmpty(properties.getAccessKey())) {
            throw new FileException("腾讯文件服务缺少accessKey配置");
        }
        if (StringUtils.isNullOrEmpty(properties.getSecurityKey())) {
            throw new FileException("腾讯文件服务缺少securityKey配置");
        }
        if (StringUtils.isNullOrEmpty(properties.getBucketName())) {
            throw new FileException("腾讯文件服务缺少bucketName配置");
        }
        if (StringUtils.isNullOrEmpty(properties.getBucketUrl())) {
            throw new FileException("腾讯文件服务缺少bucketUrl配置");
        }
        this.auth =  new BasicCOSCredentials(properties.getAccessKey(), properties.getSecurityKey());
        clientConfig= new ClientConfig(new Region(properties.getArea()));
        cosClient = new COSClient(auth, clientConfig);
        this.bucketName = properties.getBucketName();
    }
    @Deprecated
    public String upload(InputStream input, String path, String name)  {
        String key=createFileKey(path, name);
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,key,input,new ObjectMetadata());
        cosClient.putObject(putObjectRequest);
        return generatePresignedUrl(key);
    }
    /**
     *@描述 上传腾讯云文件
     *@参数  [filePath, path, name]
     *@返回值  java.lang.String
     *@创建人  霍钧城
     *@创建时间  2020/12/25 
     *@修改历史：
     */
    public String upload(String filePath, String path) {
        String key=createFileKey(path, filePath);
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,key,new File(filePath));
        cosClient.putObject(putObjectRequest);
        return generatePresignedUrl(key);
    }
    /**
     *@描述 获得通用路径
     *@参数  [key]
     *@返回值  java.lang.String
     *@创建人  霍钧城
     *@创建时间  2020/12/25
     *@修改历史：
     */
    private String generatePresignedUrl(String key){
        //默认365天有效
        long expirationTimes = PropertyUtils.getPropertyCache("bsf.file.tencent.expiration.times",  3600L * 1000 * 24 * 365);
        Date expiration = new Date(System.currentTimeMillis() + expirationTimes);
        String url = cosClient.generatePresignedUrl(bucketName, key, expiration).toString();
        url=url.substring(0,url.indexOf("?"));
        LogUtils.info(TencentFileProvider.class, FileProperties.PROVIDER_TENCENT,url);
        return url;
    }
}

package com.yh.csx.bsf.file.config;

import com.yh.csx.bsf.core.config.BsfConfiguration;
import com.yh.csx.bsf.core.util.LogUtils;
import com.yh.csx.bsf.file.FileException;
import com.yh.csx.bsf.file.FileProvider;
import com.yh.csx.bsf.file.impl.BaseFileProvider;
import com.yh.csx.bsf.file.impl.QiniuFileProvider;
import com.yh.csx.bsf.file.impl.TencentFileProvider;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


/**
 * @author huojuncheng
 **/
@Configuration
@Import(BsfConfiguration.class)
@EnableConfigurationProperties(FileProperties.class)
@ConditionalOnProperty(name = "bsf.file.enabled", havingValue = "true")
public class FileConfiguration implements InitializingBean {
    /**
     *@描述 七牛云文件初始化
     *@参数  [properties]
     *@返回值  com.yh.csx.bsf.file.impl.QiniuFileProvider
     *@创建人  霍钧城
     *@创建时间  2020/12/25
     *@修改历史：
     */
    @Bean
    public QiniuFileProvider qiniuFileProvider(FileProperties properties) {
        QiniuProperties qiniu = properties.getQiniu();
        if (qiniu == null) {
            qiniu = properties.getDefaultQiniu();
        }
        if (qiniu == null) {
            throw new FileException("不支持的文件服务提供者："+FileProperties.PROVIDER_QINIU);
        }
        QiniuFileProvider fileProvider = new QiniuFileProvider(qiniu);
        LogUtils.info(FileConfiguration.class, FileProperties.Project, FileProperties.PROVIDER_QINIU+"文件，已启动");
        return fileProvider;
    }
    /**
     *@描述 腾讯云文件初始化
     *@参数  [properties]
     *@返回值  com.yh.csx.bsf.file.impl.TencentFileProvider
     *@创建人  霍钧城
     *@创建时间  2020/12/25
     *@修改历史：
     */
    @Bean
    public TencentFileProvider tencentFileProvider(FileProperties properties) {
        TencentProperties tencent = properties.getTencent();
        if (tencent == null) {
            tencent = properties.getDefaultTencent();
        }
        if (tencent == null) {
            throw new FileException("不支持的文件服务提供者："+FileProperties.PROVIDER_TENCENT);
        }
        TencentFileProvider fileProvider = new TencentFileProvider(tencent);
        LogUtils.info(FileConfiguration.class, FileProperties.Project, FileProperties.PROVIDER_TENCENT+"文件，已启动");
        return fileProvider;
    }
    /**
     *@描述 BSF 文件服务初始化
     *@参数  []
     *@返回值  com.yh.csx.bsf.file.FileProvider
     *@创建人  霍钧城
     *@创建时间  2020/12/25
     *@修改历史：
     */
    @Bean
    public FileProvider fileProvider() {
        FileProvider fileProvider = new BaseFileProvider();
        return fileProvider;
    }
    /**
     *@描述 启动提示
     *@参数  []
     *@返回值  void
     *@创建人  霍钧城
     *@创建时间  2020/12/25
     *@修改历史：
     */
    @Override
    public void afterPropertiesSet() {
        LogUtils.info(FileConfiguration.class, FileProperties.Project, "已启动");
    }
}

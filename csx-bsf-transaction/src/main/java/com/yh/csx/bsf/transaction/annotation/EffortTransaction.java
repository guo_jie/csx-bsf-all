package com.yh.csx.bsf.transaction.annotation;

import com.yh.csx.bsf.core.base.EtTime;

import java.lang.annotation.*;
/**
 * @author huojuncheng
 */
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.PACKAGE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EffortTransaction {
    EtTime[] value() default {};
}
